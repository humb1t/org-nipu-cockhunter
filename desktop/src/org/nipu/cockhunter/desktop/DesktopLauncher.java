package org.nipu.cockhunter.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.nipu.cockhunter.CockHunter;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "CockHunter";
		config.width = 1280;
		config.height = 768;
		new LwjglApplication(new CockHunter(), config);
	}
}
