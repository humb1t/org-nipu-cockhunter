package org.nipu.cockhunter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import org.nipu.cockhunter.screens.MainMenuScreen;

public class CockHunter extends Game
{

	public SpriteBatch batch;
	public BitmapFont font;
	private Screen screen;

	public void create() {
		Assets.load();
		batch = new SpriteBatch();
		//Use LibGDX's default Arial font.
		font = new BitmapFont();
		screen = new MainMenuScreen(this);
		this.setScreen(screen);
	}

	public void render() {
		super.render(); //important!
	}

	public void dispose() {
		Assets.dispose();
		batch.dispose();
		font.dispose();
		screen.dispose();
	}
}
