package org.nipu.cockhunter.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

import org.nipu.cockhunter.Assets;

/**
 * Created by nipu0413 on 27.10.2015.
 */
public class DickActor extends BaseActor
{
	public DickActor()
	{
		this(new Sprite(Assets.dick));
	}

	public DickActor(Sprite sprite)
	{
		super(sprite, 64f, 64f);
	}

	@Override
	public Rectangle getBounds()
	{
		return super.getBounds();
	}
}
