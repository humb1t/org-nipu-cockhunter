package org.nipu.cockhunter.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;

import org.nipu.cockhunter.Assets;

/**
 * Created by nipu0413 on 30.10.2015.
 */
public class CockWithHeadActor extends Group
{
	Rectangle bounds;
	private CockActor cockActor;
	private HeadActor headActor;
	private boolean leftMove;
	private boolean rightMove;
	public CockState currentState;
	private float gravity = -800f;
	private float verticalSpeed = 0;
	private float moveSpeed = 170f;


	public enum CockState
	{
		UP, DOWN, EARTH, ON_DICK
	}

	public CockWithHeadActor()
	{
		this.cockActor = new CockActor();
		this.headActor = new HeadActor();
		headActor.setPosition(cockActor.getX() + 80f, cockActor.getY() + 80f);
		this.addActor(cockActor);
		this.addActor(headActor);
		addListener(new CockInputListener());
		this.currentState = CockState.EARTH;
		bounds = new Rectangle((int) getX(), (int) getY(), cockActor.getWidth(), cockActor.getHeight());
	}

	@Override
	public void act(float delta)
	{
		float deltaX = 0f;
		float deltaY = 0f;
		verticalSpeed += gravity * Gdx.graphics.getDeltaTime();
		if (CockState.UP.equals(currentState))
		{
			deltaY = verticalSpeed;
		} else if (CockState.DOWN.equals(currentState))
		{
			deltaY = verticalSpeed < gravity ? gravity : verticalSpeed;
		}
		if (rightMove)
		{
			deltaX = moveSpeed;
		}
		if (leftMove)
		{
			deltaX = -moveSpeed;
		}
		moveBy(deltaX * Gdx.graphics.getDeltaTime(), deltaY * Gdx.graphics.getDeltaTime());
		Array<Actor> actors = super.getStage().getActors();
		for (Actor actor : actors)
		{
			if (actor instanceof PlatformActor)
			{
				Rectangle platformBounds = ((PlatformActor) actor).getBounds();
				Rectangle footBounds = this.getBounds().setHeight(1f);
				if (platformBounds.overlaps(footBounds) && footBounds.getY() >= (platformBounds.getY() + platformBounds.getHeight() - 15))
				{
					this.currentState = CockState.EARTH;
					break;
				} else
				{
					this.currentState = CockState.DOWN;
				}
			}
			if (actor instanceof DickActor)
			{
				Rectangle dickBounds = ((DickActor) actor).getBounds();
				if (!dickBounds.overlaps(this.getBounds()))
					continue;
				this.currentState = CockState.ON_DICK;
				Assets.dick_catched.play();
				this.getStage().getActors().removeValue(actor, false);
				this.currentState = CockState.DOWN;
			}
		}
		super.act(delta);
	}

	public void setLeftMove(boolean t)
	{
		if (rightMove && t) rightMove = false;
		leftMove = t;
	}

	public void setRightMove(boolean t)
	{
		if (leftMove && t) leftMove = false;
		rightMove = t;
	}

	private class CockInputListener extends InputListener
	{

		@Override
		public boolean keyDown(InputEvent event, int keycode)
		{
			switch (keycode)
			{
				case Input.Keys.RIGHT:
					setRightMove(true);
					break;
				case Input.Keys.LEFT:
					setLeftMove(true);
					break;
				case Input.Keys.UP:
					if (CockState.EARTH.equals(currentState))
					{
						currentState = CockState.UP;
						verticalSpeed = 460f;
					}
					break;
				case Input.Keys.DOWN:
					currentState = CockState.DOWN;
			}
			return true;
		}

		@Override
		public boolean keyUp(InputEvent event, int keycode)
		{
			switch (keycode)
			{
				case Input.Keys.LEFT:
					setLeftMove(false);
					break;
				case Input.Keys.RIGHT:
					setRightMove(false);
					break;
				case Input.Keys.UP:
					currentState = CockState.DOWN;
					break;
			}
			return true;
		}
	}

	public Rectangle getBounds()
	{
		return bounds;
	}

	@Override
	protected void positionChanged()
	{
		bounds.setX((int) getX());
		bounds.setY((int) getY());
		super.positionChanged();
	}
}
