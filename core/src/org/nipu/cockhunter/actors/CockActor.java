package org.nipu.cockhunter.actors;

import com.badlogic.gdx.graphics.g2d.Sprite;

import org.nipu.cockhunter.Assets;

/**
 * Created by nipu0413 on 27.10.2015.
 */
public class CockActor extends BaseActor
{

	public CockActor()
	{
		this(new Sprite(Assets.cock));
	}

	public CockActor(Sprite sprite)
	{
		super(sprite, 128f, 128f);
	}
}
