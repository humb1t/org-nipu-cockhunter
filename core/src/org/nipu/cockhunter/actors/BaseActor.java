package org.nipu.cockhunter.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

/**
 * Created by nipu0413 on 26.10.2015.
 */
public class BaseActor extends Actor
{
	Sprite sprite;
	Rectangle bounds;

	public BaseActor(Sprite sprite, float sizeX, float sizeY)
	{
		this.sprite = sprite;
		this.sprite.setSize(sizeX, sizeY);
		setBounds(sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());
		setTouchable(Touchable.enabled);
		bounds=new Rectangle((int)getX(), (int)getY(), (int)getWidth(), (int)getHeight());
	}

	@Override
	protected void positionChanged()
	{
		sprite.setPosition(getX(), getY());
		bounds.setX((int) getX());
		bounds.setY((int) getY());
		super.positionChanged();
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		sprite.draw(batch);
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
	}

	public Rectangle getBounds() {
		return bounds;
	}
}
