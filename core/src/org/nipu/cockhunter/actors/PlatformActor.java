package org.nipu.cockhunter.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

import org.nipu.cockhunter.Assets;

/**
 * Created by nipu0413 on 24.11.2015.
 */
public class PlatformActor extends BaseActor
{
	public PlatformActor()
	{
		this(new Sprite(Assets.platform, 426, 96));
	}

	public PlatformActor(Sprite sprite)
	{
		super(sprite, 426f, 30f);
		sprite.setColor(Color.WHITE);
	}

	@Override
	public Rectangle getBounds()
	{
		return super.getBounds();
	}
}
