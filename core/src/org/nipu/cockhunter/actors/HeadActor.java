package org.nipu.cockhunter.actors;

import com.badlogic.gdx.graphics.g2d.Sprite;

import org.nipu.cockhunter.Assets;

/**
 * Created by nipu0413 on 30.10.2015.
 */
public class HeadActor extends BaseActor
{
	public HeadActor()
	{
		this(new Sprite(Assets.cock_head), 64, 64);
	}

	public HeadActor(Sprite sprite, float sizeX, float sizeY)
	{
		super(sprite, sizeX, sizeY);
	}
}
