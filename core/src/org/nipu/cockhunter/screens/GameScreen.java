package org.nipu.cockhunter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import org.nipu.cockhunter.CockHunter;
import org.nipu.cockhunter.actors.CockActor;
import org.nipu.cockhunter.actors.CockWithHeadActor;
import org.nipu.cockhunter.actors.DickActor;
import org.nipu.cockhunter.actors.PlatformActor;

import java.util.List;

/**
 * Created by nipu0413 on 27.10.2015.
 */
public class GameScreen implements Screen
{
	final CockHunter game;
	private Stage stage;
	public Array<PlatformActor> platformActors = new Array<PlatformActor>();
	public Array<DickActor> dickActors = new Array<DickActor>();
	public CockWithHeadActor player;

	public GameScreen(CockHunter game)
	{
		this.game = game;
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);

		player = new CockWithHeadActor();
		player.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());
		stage.addActor(player);
		stage.setKeyboardFocus(player);
		for (int i = 0; i < 10; i++)
		{
			DickActor tempDick = new DickActor();
			float randX = MathUtils.random(50, Gdx.graphics.getWidth()-50);
			tempDick.setPosition(randX, i*110);
			tempDick.setName("dick" + i);
			dickActors.add(tempDick);
			stage.addActor(tempDick);

			PlatformActor platformActor = new PlatformActor();
			platformActor.setPosition(randX, i*100);
			platformActor.setName("platform" + i);
			platformActors.add(platformActor);
			stage.addActor(platformActor);
		}
		stage.setDebugAll(true);
	}

	@Override
	public void show()
	{
	}

	@Override
	public void render(float delta)
	{
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height)
	{
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void hide()
	{

	}

	@Override
	public void dispose()
	{
		stage.dispose();
	}
}
